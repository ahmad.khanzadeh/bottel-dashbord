import './App.css'
import Header from './assets/components/Header'
import Sidebar from './assets/components/Sidebar'
import Home from './assets/components/Home'
import { useState } from 'react'

function App() {
  const [openSidebarToggle, setOpenSidebarToggle] = useState(false);

  const OpenSidebar =() => {
    setOpenSidebarToggle(!openSidebarToggle);
  }
  
  return (
    <div className="gird-container">
      <Header OpenSidebar={OpenSidebar} />
      <Sidebar openSidebarToggle={openSidebarToggle} OpenSidebar={OpenSidebar}/>
      <Home />
     </div>
  )
}

export default App
