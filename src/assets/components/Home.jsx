import React from 'react'
import { BsGrid1X2Fill, BsFillArchiveFill, BsFillGrid3X2GapFill, BsListCheck, BsMenuButtonWideFill, BsFillBellFill , BsPeopleFill } from 'react-icons/bs';
import { BarChart, Bar, Rectangle, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { LineChart, Line } from 'recharts';


const Home = () => {
  // data from simplechart.org
  const data = [
    {
      name: 'Barranco',
      glasses: 4000,
      plastic: 2400,
      amt: 2400,
    },
    {
      name: 'Avenida Jose Larco',
      glasses: 3000,
      plastic: 1398,
      amt: 2210,
    },
    {
      name: 'Miraflores',
      glasses: 2000,
      plastic: 9800,
      amt: 2290,
    },
    {
      name: ' Avenida Javier Prado',
      glasses: 2780,
      plastic: 3908,
      amt: 2000,
    },
    {
      name: 'Jirón de la Unión',
      glasses: 1890,
      plastic: 4800,
      amt: 2181,
    },
    {
      name: 'Miraflores',
      glasses: 2390,
      plastic: 3800,
      amt: 2500,
    },
    {
      name: 'Paseo de la Republica',
      glasses: 3490,
      plastic: 4300,
      amt: 2100,
    },
  ];

  return (
    <main>
      <div className='main-container'>
        <div className='main-title'>
          <h3>Vending machine Admin Panel</h3>
        </div>

        <div className='main-cards'>
            <div className='card'>
              <div className='card-inner'>
                <h3>Different Bottel Size</h3>
                <BsFillArchiveFill className='card_icon' />
              </div>
              <h1>6</h1>
            </div>

            <div className='card'>
              <div className='card-inner'>
                <h3>Different Brands</h3>
                <BsGrid1X2Fill className='card_icon' />
              </div>
              <h1>20</h1>
            </div>

            <div className='card'>
              <div className='card-inner'>
                <h3>Machine Location</h3>
                <BsPeopleFill  className='card_icon' />
              </div>
              <h1>8</h1>
            </div>

            <div className='card'>
              <div className='card-inner'>
                <h3>Allerts</h3>
                <BsFillBellFill  className='card_icon' />
              </div>
              <h1>11</h1>
            </div>

        </div>
      </div>
      
      <div className="charts">
      {/*  the bar chart */}
      <ResponsiveContainer width="100%" height="100%">
        <BarChart
          width={500}
          height={300}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="plastic" fill="#8884d8" activeBar={<Rectangle fill="pink" stroke="blue" />} />
          <Bar dataKey="glasses" fill="#82ca9d" activeBar={<Rectangle fill="gold" stroke="purple" />} />
        </BarChart>
      </ResponsiveContainer>
      {/* line chart */}
      <ResponsiveContainer width="100%" height="100%">
        <LineChart
          width={500}
          height={300}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="plastic" stroke="#8884d8" activeDot={{ r: 8 }} />
          <Line type="monotone" dataKey="glasses" stroke="#82ca9d" />
        </LineChart>
      </ResponsiveContainer>

      </div>
    </main>
  )
}

export default Home