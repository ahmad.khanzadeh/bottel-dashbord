import React from 'react'
import { BsFillEnvelopeAtFill, BsPersonCircle, BsSearch, BsJustify, BsFillBellFill} from 'react-icons/bs'

const Header = ({OpenSidebar}) => {
  return (
    <div className="header">
        <div className="menu-icon">
        {/* in mobile view, this will have the function to toggle the menu */}
            <BsJustify className="icon" onClick={OpenSidebar}/>
        </div>
        <div className="header-left">
            <BsJustify className='icon'/>
        </div>
        <div className="header-right">
            <BsFillBellFill className="icon" />
            <BsFillEnvelopeAtFill className="icon" />
            <BsPersonCircle className="icon" />
        </div>
    </div>
  )
}

export default Header